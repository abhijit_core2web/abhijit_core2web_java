import java.util.Scanner;
class ArrayD8D{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the Size: ");
		int size = sc.nextInt();

		char arr[] = new char[size];
		System.out.println("Enter the character: ");
		for(int i=0; i<arr.length; i++){

			arr[i] = sc.next().charAt(0);
		}
		System.out.println("Enter the character to check: ");
		char character = sc.next().charAt(0);

                int count = 0;
		for(int i=0; i<arr.length; i++){

			if(arr[i] == character){

				count= count + 1;
			}
		}
		System.out.println(character+"occurs"+count+"times in an array");
	}
}

		
