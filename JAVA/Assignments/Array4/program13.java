import java.util.Scanner;
class Array9D9{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size: ");
		int size = sc.nextInt();

		char arr[] = new char[size];
		System.out.println("Enter the Elements: ");
		for(int i=0; i<arr.length; i++){

			arr[i] = sc.next().charAt(0);
		}
		for(int i=0; i<arr.length; i++){

			if(arr[i]>=42 && arr[i]<90){

				System.out.println("#");
			}
			else{
				System.out.println(arr[i]);
			}
		}
	}
}

