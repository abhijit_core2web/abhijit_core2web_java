

class p8 {
	public static void main(String[] args) {
		int num = 22;
		if (num % 2 ==0 && num % 5 ==0 && num % 10==0) {
			System.out.println("Divisible by all");
		} else if(num % 5 ==0) {
			System.out.println("Divisible only by 5");
		} else if(num%2 ==0){
			System.out.println("Divisible only by 2");
		} else {
			System.out.println("Not divisble by 2,5,10");
		}
	}
}
