



class p18 {
	public static void main(String[] args) {
		String friends = "Krrish";
		System.out.println("Before Switch");
		switch(friends) {
			case "Ranjeet":
					System.out.println("Ranjeet");
					break;
			case "Raj"    :
					System.out.println("Raj");
					break;
			case "Krrish" :
					System.out.println("Krrish");
					break;
			case "Bablu"  :
					System.out.println("Bablu");
					break;
			default       :
					System.out.println("In Default State");
		}
		System.out.println("After Switch");

	}
}
